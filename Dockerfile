FROM eu.gcr.io/vseth-public/base:echo

RUN apt install -y curl
RUN curl -sL https://deb.nodesource.com/setup_16.x | bash -
RUN apt install -y nodejs

RUN mkdir -p /tasting
WORKDIR /tasting
COPY . /tasting

RUN npm install
RUN npm run build

COPY cinit.yml /etc/cinit.d/tasting.yml

EXPOSE 3000
