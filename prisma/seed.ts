// prisma/seed.ts
import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();

async function main() {
  await prisma.brewery.create({
    data: {
      name: "Brasserie Les Trois Dames",
      descriptionDE: "Is Nice",
      descriptionEN: "Is Nice",
      homepage: "https://aschoch.ch",
      location: "Sainte-Croix VD",
      beers: {
        create: [
          {
            name: "Envoutée",
            type: "Sour",
            descriptionDE: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a",
            descriptionEN: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a",
            alc: "5.2",
            slug: "envoutee",
            price: "2",
            is_tapped: true,
            comments: {
              create: [
                {
                  first_name: "Alexander",
                  last_name: "Schoch",
                  comment: "Taste Nice!",
                }
              ]
            }
          },
          {
            name: "Lager",
            type: "Lager",
            descriptionDE: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes,",
            descriptionEN: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes,",
            alc: "4.7",
            slug: "lager",
            price: "1.50",
            is_tapped: true,
          },
          {
            name: "Amber",
            type: "Amber",
            descriptionDE: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.",
            descriptionEN: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.",
            alc: "4.2",
            slug: "amber",
            price: "2.50",
            is_tapped: true,
          },
        ]
      }
    }
  });

  await prisma.brewery.create({
    data: {
      name: "BFM",
      descriptionDE: "Is Nice",
      descriptionEN: "Is Nice",
      homepage: "https://bfm.ch",
      location: "Saignelégier JU",
      beers: {
        create: [
          {
            name: "IPA",
            type: "IPA",
            descriptionDE: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a",
            descriptionEN: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a",
            alc: "5.2",
            slug: "ipa",
            price: "2",
            is_tapped: true,
          },
          {
            name: "Tripel",
            type: "Belgian Tripel",
            descriptionDE: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.",
            descriptionEN: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.",
            alc: "4.7",
            slug: "tripel",
            price: "1.50",
            is_tapped: true,
          },
          {
            name: "Stout",
            type: "Imperial Stout",
            descriptionDE: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes,",
            descriptionEN: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes,",
            alc: "4.2",
            slug: "stout",
            price: "2.50",
            is_tapped: true,
          },
        ]
      }
    }
  });
}

main()
  .catch((e) => {
    console.error(e);
    process.exit(1);
  })
  .finally(async () => {
    await prisma.$disconnect();
  });
