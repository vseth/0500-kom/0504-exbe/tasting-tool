/*
  Warnings:

  - You are about to drop the column `description` on the `Beer` table. All the data in the column will be lost.
  - You are about to drop the column `description` on the `Brewery` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE `Beer` DROP COLUMN `description`,
    ADD COLUMN `descriptionDE` TEXT NULL,
    ADD COLUMN `descriptionEN` TEXT NULL;

-- AlterTable
ALTER TABLE `Brewery` DROP COLUMN `description`,
    ADD COLUMN `descriptionDE` TEXT NULL,
    ADD COLUMN `descriptionEN` TEXT NULL;
