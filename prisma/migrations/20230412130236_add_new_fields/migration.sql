/*
  Warnings:

  - Made the column `homepage` on table `Brewery` required. This step will fail if there are existing NULL values in that column.

*/
-- AlterTable
ALTER TABLE `Beer` ADD COLUMN `is_tapped` BOOLEAN NOT NULL DEFAULT false;

-- AlterTable
ALTER TABLE `Brewery` ADD COLUMN `location` VARCHAR(191) NULL,
    MODIFY `homepage` VARCHAR(191) NOT NULL;
