/*
  Warnings:

  - Made the column `slug` on table `Beer` required. This step will fail if there are existing NULL values in that column.

*/
-- AlterTable
ALTER TABLE `Beer` MODIFY `slug` VARCHAR(191) NOT NULL;
