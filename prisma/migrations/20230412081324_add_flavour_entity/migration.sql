-- CreateTable
CREATE TABLE `Flavour` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `sub` VARCHAR(191) NULL,
    `hoppy` DOUBLE NULL,
    `malty` DOUBLE NULL,
    `fruity` DOUBLE NULL,
    `bitter` DOUBLE NULL,
    `sweet` DOUBLE NULL,
    `sour` DOUBLE NULL,
    `beerId` INTEGER NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `Flavour` ADD CONSTRAINT `Flavour_beerId_fkey` FOREIGN KEY (`beerId`) REFERENCES `Beer`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;
