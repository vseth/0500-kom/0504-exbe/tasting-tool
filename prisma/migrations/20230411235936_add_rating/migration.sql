-- CreateTable
CREATE TABLE `Rating` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `rating` DOUBLE NULL,
    `sub` VARCHAR(191) NULL,
    `beerId` INTEGER NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `Rating` ADD CONSTRAINT `Rating_beerId_fkey` FOREIGN KEY (`beerId`) REFERENCES `Beer`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;
