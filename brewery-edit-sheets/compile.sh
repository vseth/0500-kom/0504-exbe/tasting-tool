#!/bin/bash

DATA=data/QR_Data.csv
OUTPUT=output/
QR=qrcodes/
TEX=tex/editSheets.tex

mkdir -p tmp
cp tex/icons tmp -r

while read line
do
	brewery=$( echo $line | cut -d ',' -f 1 )
	uuid=$( echo $line | cut -d ',' -f 2 )

  brewery_esc=$( echo $brewery | sed 's/ /_/g; s/\.//g' )

	echo "Processing $brewery"

	url="https://tasting.exbeerience.ethz.ch/edit/$uuid"
	url_esc=$( echo "$url" | sed 's/\//\\\//g' | sed 's/_/\\\\_/g' )

	cd "$QR"
	qrencode "$url" -m 0 -t SVG -o "$uuid.svg"
  python -m cairosvg "$uuid.svg" -o "$uuid.pdf"
	#inkscape "$uuid.svg" --export-type=pdf -o "$uuid.pdf"
	rm "$uuid.svg"
	cd '..'

	cp $TEX "tmp/$uuid.tex"
	cd tmp
	sed -i "s/BREWERY/$brewery/g" "$uuid.tex"
	sed -i "s/UUID/$uuid/g" "$uuid.tex"

	sed -i "s/URL/$url_esc/g" "$uuid.tex"
	lualatex "$uuid.tex" > /dev/null
	mv $uuid.pdf "../output/$brewery_esc.pdf"
	cd '..'
	
done < "$DATA"

rm -r tmp
