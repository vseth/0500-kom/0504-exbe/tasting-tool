import { ReactNode } from "react";
import { useRouter } from "next/router";
import Link from "next/link";
import { useTranslation } from "next-i18next";

import {
  Alert,
  Blockquote,
  Button,
  Card,
  Group,
  List,
  Skeleton,
  Space,
  Text,
} from "@mantine/core";

import { Icon, ICONS } from "vseth-canine-ui";

import BreweryType from "../interfaces/brewery";

export interface BreweryCardProps {
  brewery: BreweryType;
  beerSlug?: string | null;
  admin?: boolean;
  editButton?: ReactNode | null;
  deleteButton?: ReactNode | null;
}

export default function BreweryCard({
  brewery,
  beerSlug = null,
  admin = false,
  editButton = null,
  deleteButton = null,
}: BreweryCardProps) {
  const { locale } = useRouter();
  const { t } = useTranslation("comp.breweryCard");

  return (
    <Card
      shadow="md"
      radius="md"
      style={{
        border: "1px solid #fcdbb0",
        display: "flex",
        justifyContent: "space-between",
        flexDirection: "column",
        width: "100%",
      }}
    >
      <div>
        <h1>{brewery.name}</h1>
        <h2>
          {t("from")} {brewery.location}
        </h2>

        <Space h="md" />

        {!brewery.is_tapped_by_brewery && (
          <>
            <Alert
              icon={<Icon icon={ICONS.INFO} />}
              title={t("notTappedByTitle")}
              color="blue"
            >
              {t("notTappedByText")}
            </Alert>
            <Space h="md" />
          </>
        )}

        <Text style={{ whiteSpace: "pre-wrap" }}>
          {locale === "en" ? brewery.descriptionEN : brewery.descriptionDE}
        </Text>

        <Space h="md" />

        {admin ? (
          <p>
            {t("beersBy")} {brewery.name}:
          </p>
        ) : (
          <p>
            {t("otherBeersBy")} {brewery.name}:
          </p>
        )}
        <List withPadding>
          {brewery.beers
            .filter((beer) => beer.slug != beerSlug)
            .map((beer) => (
              <List.Item key={beer.slug}>
                <Link href={"/beer/" + beer.slug} style={{ color: "black" }}>
                  {beer.name} ({beer.type}
                  {!beer.is_tapped ? t("notAvail") : ""})
                </Link>
              </List.Item>
            ))}
        </List>
      </div>

      <div>
        <Space h="md" />
        <Button
          variant="light"
          rightIcon={<Icon icon={ICONS.SEND_ROUND} color="#ad6300" />}
          component="a"
          href={brewery.homepage}
          target="_blank"
        >
          {t("website")}
        </Button>

        {admin && (
          <>
            <Space h="md" />
            <Group grow>
              {editButton}
              {deleteButton}
            </Group>
          </>
        )}
      </div>
    </Card>
  );
}
