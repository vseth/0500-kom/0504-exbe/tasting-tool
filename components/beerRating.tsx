import { useState, useEffect } from "react";
import { useSession, signIn, signOut } from "next-auth/react";

import { useTranslation } from "next-i18next";

import { Accordion, Box, Button, Container, Space } from "@mantine/core";

/*
 * @mantine/core's Rating component has a bug on mobile that it is not
 * displaying its value correctly. For example, if the value is 4, the
 * component might display 3.5 stars.
 */
import CircularSlider from "@fseehawer/react-circular-slider";

import { Icon, ICONS } from "vseth-canine-ui";

import PleaseLogIn from "../components/pleaseLogIn";

import { gql, useMutation } from "@apollo/client";

const MUTATION = gql`
  mutation rating($sub: String!, $rating: Float!, $beerId: Int!) {
    rating(sub: $sub, rating: $rating, beerId: $beerId) {
      id
    }
  }
`;

const MUTATION_UPDATE = gql`
  mutation updateRating($id: Int!, $rating: Float!) {
    updateRating(id: $id, rating: $rating) {
      id
    }
  }
`;

import RatingType from "../interfaces/rating";
import BeerType from "../interfaces/beer";

export interface BeerRatingProps {
  beerId: number;
  refetch: any;
  ratings: [RatingType];
  notify: (title: string, message: string, color: string) => void;
  update: number;
}

export default function BeerRating({
  beerId,
  refetch,
  ratings,
  notify,
  update,
}: BeerRatingProps) {
  const { t } = useTranslation("comp.beerRating");

  const { data: session } = useSession();
  const [insertRating, { loading, error }] = useMutation(MUTATION);
  const [updateRating, { loading: loading_upd, error: error_upd }] =
    useMutation(MUTATION_UPDATE);

  const [rating, setRating] = useState<number>(0);
  const [found, setFound] = useState<RatingType[]>([]);
  const [foundOne, setFoundOne] = useState(false);

  const reloadRating = () => {
    if (session && ratings) {
      const foundLoc = ratings.filter(
        (rating) => rating.sub == (session as any).info.payload.sub
      );
      setFound(foundLoc);
      if (foundLoc.length > 0) {
        setRating(foundLoc[0].rating);
        setFoundOne(true);
      }
    }
  };

  useEffect(() => {
    reloadRating();
  }, [update]);

  const submit = async () => {
    if (!foundOne) {
      await insertRating({
        variables: {
          sub: (session as any).info.payload.sub,
          rating: rating,
          beerId: beerId,
        },
      });
      notify(t("ratingAdded"), "", "green");
    } else {
      await updateRating({
        variables: {
          id: found[0].id,
          rating: rating,
        },
      });
      notify(t("ratingUpdated"), "", "green");
    }

    await refetch();
  };

  return (
    <Accordion.Item value="rating">
      <Accordion.Control>
        <h2>{t("title")}</h2>
      </Accordion.Control>
      <Accordion.Panel>
        <Space h="md" />
        {session ? (
          <div style={{ textAlign: "center" }}>
            <Box>
              <CircularSlider
                onChange={(val: number) => setRating(val)}
                label={t("rating")}
                min={0}
                max={5}
                data={Array(51)
                  .fill(1)
                  .map((element, index) => index / 10)}
                dataIndex={Math.round(rating * 10)}
                knobColor="#ad6300"
                progressColorFrom="#ff8500"
                progressColorTo="#ad6300"
                progressSize={8}
                trackColor="#fcefde"
                labelColor="#ad6300"
              />
            </Box>
            <Button style={{ marginTop: "10px" }} onClick={submit}>
              {foundOne ? t("update") : t("submit")}
            </Button>
          </div>
        ) : (
          <>
            <PleaseLogIn />
          </>
        )}
        <Space h="md" />
      </Accordion.Panel>
    </Accordion.Item>
  );
}
