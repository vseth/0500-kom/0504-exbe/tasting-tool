import { useState } from "react";

import { Button, Grid, Space } from "@mantine/core";

import { useTranslation } from "next-i18next";

import { Icon, ICONS } from "vseth-canine-ui";

import BeerForm from "../components/beerForm";
import DeleteDialog from "../components/deleteDialog";
import BeerCard from "../components/beerCard";

import BeerType from "../interfaces/beer";
import BreweryType from "../interfaces/brewery";

export interface AdminBeersProps {
  beers: [BeerType];
  breweries: [BreweryType];
  refetchBreweries: any;
  refetchBeers: any;
  uuid?: string | string[];
}

export default function AdminBeers({
  beers,
  breweries,
  refetchBreweries,
  refetchBeers,
  uuid = "",
}: AdminBeersProps) {
  const { t } = useTranslation("comp.adminBeers");

  const [currentBeer, setCurrentBeer] = useState<BeerType | null>(null);
  const [beerFormOpen, setBeerFormOpen] = useState<boolean>(false);

  const [isBeerSelected, setIsBeerSelected] = useState<boolean>(false);
  const [isDeleteFormOpen, setIsDeleteFormOpen] = useState<boolean>(false);

  return (
    <>
      <Button
        leftIcon={<Icon icon={ICONS.PLUS} color="#ffffff" />}
        onClick={() => {setCurrentBeer(null); setBeerFormOpen(true)}}
      >
        {t("addBeer")}
      </Button>

      <Space h="md" />

      <Grid>
        {beers &&
          beers.map((beer) => (
            <Grid.Col
              md={4}
              sm={6}
              xs={12}
              style={{ display: "flex" }}
              key={beer.id}
            >
              <BeerCard
                beer={beer}
                showLink={false}
                admin={true}
                editButton={
                  <Button
                    leftIcon={<Icon icon={ICONS.EDIT} color="#ffffff" />}
                    onClick={() => {
                      setCurrentBeer(beer);
                      setBeerFormOpen(true);
                    }}
                  >
                    {t("edit")}
                  </Button>
                }
                deleteButton={
                  <Button
                    leftIcon={<Icon icon={ICONS.DELETE} color="#ffffff" />}
                    color="red"
                    onClick={() => {
                      setCurrentBeer(beer);
                      setIsBeerSelected(true);
                      setIsDeleteFormOpen(true);
                    }}
                  >
                    {t("delete")}
                  </Button>
                }
              />
            </Grid.Col>
          ))}
      </Grid>

      {breweries && (
        <BeerForm
          open={beerFormOpen}
          beer={currentBeer}
          close={() => {
            setBeerFormOpen(false);
            setCurrentBeer(null);
          }}
          breweries={breweries}
          refetch={refetchBeers}
          uuid={uuid}
        />
      )}

      <DeleteDialog
        open={isDeleteFormOpen}
        close={() => setIsDeleteFormOpen(false)}
        beer={currentBeer}
        setCurrentBeer={setCurrentBeer}
        isBeerSelected={true}
        refetchBeers={refetchBeers}
        refetchBreweries={refetchBreweries}
        uuid={uuid}
      />
    </>
  );
}
