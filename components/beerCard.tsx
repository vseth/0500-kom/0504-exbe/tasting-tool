import { ReactNode } from "react";
import { useRouter } from "next/router";
import Link from "next/link";
import { useTranslation } from "next-i18next";

import {
  Alert,
  Blockquote,
  Button,
  Card,
  Group,
  List,
  Rating,
  Skeleton,
  Space,
  Text,
} from "@mantine/core";

import { Icon, ICONS } from "vseth-canine-ui";

import BeerType from "../interfaces/beer";
import RatingType from "../interfaces/rating";

export interface BeerCardProps {
  beer?: BeerType | null;
  showLink?: boolean;
  admin?: boolean;
  editButton?: ReactNode | null;
  deleteButton?: ReactNode | null;
}

export default function BeerCard({
  beer = null,
  showLink = true,
  admin = false,
  editButton = null,
  deleteButton = null,
}: BeerCardProps) {
  const { locale } = useRouter();
  const { t } = useTranslation("comp.beerCard");

  const rating = (ratings: [RatingType]) => {
    let sum = 0;
    ratings.forEach((rating) => {
      sum += rating.rating;
    });
    if (ratings.length > 0) return sum / ratings.length;
    return 0;
  };

  if (!beer) {
    return (
      <Card shadow="md" radius="md">
        <Skeleton height={50} width={150} radius="xl" mb="xl" />
        <Skeleton height={20} width={100} radius="xl" mb="xl" />
        <Skeleton height={8} radius="xl" />
        <Skeleton height={8} mt={6} radius="xl" />
        <Skeleton height={8} mt={6} width="70%" radius="xl" />
        <Skeleton height={30} width={70} radius="xl" mt="xl" />
      </Card>
    );
  }

  return (
    <Card
      shadow="md"
      radius="md"
      style={{
        border: "1px solid #fcdbb0",
        display: "flex",
        justifyContent: "space-between",
        flexDirection: "column",
        width: "100%",
      }}
    >
      <div>
        <h1>{beer.name}</h1>
        {beer.brewery && <h2>{beer.brewery.name}</h2>}

        <Space h="md" />

        {!beer.is_tapped && (
          <>
            <Alert
              icon={<Icon icon={ICONS.INFO} />}
              title={t("notAvailableTitle")}
              color="red"
            >
              {t("notAvailableText")}
            </Alert>
            <Space h="md" />
          </>
        )}

        <List spacing="xs" size="md" center>
          <List.Item
            icon={<Icon icon={ICONS.BEER} color={"#ad6300"} size={22} />}
          >
            <p>{beer.type}</p>
          </List.Item>
          <List.Item
            icon={<Icon icon={ICONS.MONEY} color={"#ad6300"} size={22} />}
          >
            <p>CHF {beer.price}</p>
          </List.Item>
          <List.Item
            icon={<Icon icon={ICONS.WINE} color={"#ad6300"} size={22} />}
          >
            <p>{beer.alc} %</p>
          </List.Item>
          <List.Item
            icon={
              <Icon icon={ICONS.STAR_HALF_FILLED} color={"#ad6300"} size={22} />
            }
          >
            <Rating value={rating(beer.ratings)} fractions={2} readOnly />
          </List.Item>
        </List>

        <Space h="md" />

        <Text style={{ whiteSpace: "pre-wrap" }}>
          {locale === "en" ? beer.descriptionEN : beer.descriptionDE}
        </Text>
      </div>

      {showLink && (
        <div>
          <Space h="md" />
          <Link href={"/beer/" + beer.slug}>
            <Button
              variant="light"
              rightIcon={<Icon icon={ICONS.RIGHT} color="#ad6300" />}
            >
              {t("viewThisBeer")}
            </Button>
          </Link>
        </div>
      )}

      {admin && (
        <>
          <Space h="md" />
          <Group grow>
            {editButton}
            {deleteButton}
          </Group>
        </>
      )}
    </Card>
  );
}
