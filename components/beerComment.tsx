import React from "react";

import { useSession } from "next-auth/react";

import { useTranslation } from "next-i18next";

import { Accordion, Button, Container, Space, TextInput } from "@mantine/core";

import { useInputState } from "@mantine/hooks";

import PleaseLogIn from "../components/pleaseLogIn";

import { gql, useMutation } from "@apollo/client";

const MUTATION = gql`
  mutation comment(
    $first_name: String!
    $last_name: String!
    $comment: String!
    $beerId: Int!
  ) {
    comment(
      first_name: $first_name
      last_name: $last_name
      comment: $comment
      beerId: $beerId
    ) {
      id
    }
  }
`;

type ButtonEvent = React.MouseEvent<HTMLButtonElement>;
import BeerType from "../interfaces/beer";

export interface BeerCommentProps {
  beerId: number;
  refetch: any;
  notify: (title: string, message: string, color: string) => void;
}

export default function BeerComment({
  beerId,
  refetch,
  notify,
}: BeerCommentProps) {
  const { data: session } = useSession();
  const [insertComment, { loading, error }] = useMutation(MUTATION);

  const { t } = useTranslation("comp.beerComment");

  const [comment, setComment] = useInputState("");

  const submit = async (e: ButtonEvent) => {
    e.preventDefault();
    if (!session) return;
    if (comment == "") return;
    await insertComment({
      variables: {
        first_name: (session as any).info.payload.given_name,
        last_name: (session as any).info.payload.family_name,
        comment: comment,
        beerId: beerId,
      },
    });
    await refetch();
    setComment("");
    notify("Comment Added", "", "green");
  };

  return (
    <Accordion.Item value="comment">
      <Accordion.Control>
        <h2>{t("title")}</h2>
      </Accordion.Control>
      <Accordion.Panel>
        <Space h="md" />
        {session ? (
          <form>
            <TextInput
              value={comment}
              onChange={setComment}
              placeholder={t("placeholder")}
            />
            <Button
              type="submit"
              style={{ marginTop: "10px" }}
              onClick={submit}
            >
              {t("submit")}
            </Button>
          </form>
        ) : (
          <>
            <PleaseLogIn />
          </>
        )}
        <Space h="md" />
      </Accordion.Panel>
    </Accordion.Item>
  );
}
