import Link from "next/link";

import { useTranslation } from "next-i18next";

import { Button, Container, Space } from "@mantine/core";

import { Icon, ICONS } from "vseth-canine-ui";

export default function Forbidden() {
  const { t } = useTranslation("comp.forbidden");
  return (
    <Container size="xl" sx={{ textAlign: "center" }}>
      <h1>{t("forbidden")}</h1>
      <Space h="xl" />
      <Link href="/">
        <Button leftIcon={<Icon icon={ICONS.LEFT} color="white" />}>
          {t("takeMeBack")}
        </Button>
      </Link>
    </Container>
  );
}
