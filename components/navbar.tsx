import React, { ReactNode } from "react";
import Link from "next/link";
import { useRouter } from "next/router";

import { useSession } from "next-auth/react";

import { ApolloProvider } from "@apollo/client";
import apolloClient from "../lib/apollo";

import { NotificationsProvider } from "@mantine/notifications";

import { Group, Switch, useMantineColorScheme } from "@mantine/core";

import {
  makeVsethTheme,
  useConfig,
  VSETHExternalApp,
  VSETHThemeProvider,
  Icon,
  ICONS,
} from "vseth-canine-ui";

import LoginButton from "../components/loginButton";

import hasAccess from "../utilities/hasAccess";

export default function Navbar(props: any) {
  const { locale } = useRouter();
  const router = useRouter();
  const { pathname, asPath, query } = router;

  const { data: session } = useSession();

  const theme = makeVsethTheme();
  const [selectedLanguage, setSelectedLanguage] = React.useState(locale);
  // const { colorScheme, toggleColorScheme } = useMantineColorScheme();
  // const dark = colorScheme === 'dark';

  let { data } = useConfig(
    "https://static.vseth.ethz.ch/assets/vseth-0504-exbe/config.json"
  );

  if (theme) {
    // theme.colorScheme = dark ? 'dark' : 'light';

    if (theme.colors) {
      theme.colors.vsethMain![0] = "#fcefde"; // light button background
      theme.colors.vsethMain![1] = "#fcdbb0"; // light button background, hover
      theme.colors.vsethMain![7] = "#ad6300"; // normal
      theme.colors.vsethMain![8] = "#fa9800"; // light
    }
  }

  const customDemoNav = [
    { title: "Main", href: "/" },
    { title: "Beers", href: "/beers" },
  ];

  if (hasAccess(session, true)) {
    customDemoNav.push({ title: "Admin", href: "/admin" });
  }

  return (
    <React.StrictMode>
      <VSETHThemeProvider theme={theme}>
        <VSETHExternalApp
          selectedLanguage={selectedLanguage}
          onLanguageSelect={(lang: string) => {
            setSelectedLanguage(lang);
            router.push({ pathname, query }, asPath, { locale: lang });
          }}
          languages={data?.languages}
          title="Tasting Tool"
          appNav={customDemoNav}
          organizationNav={data.externalNav}
          makeWrapper={(url: string | undefined, child: ReactNode) => (
            <Link
              href={url!}
              style={{ textDecoration: "none", color: "inherit" }}
            >
              {child}
            </Link>
          )}
          privacyPolicy={data?.privacy}
          disclaimer={data?.copyright}
          //activeHref is the current active url path. Required to style the active page in the Nav
          activeHref={"/"}
          socialMedia={data?.socialMedia}
          //logo="https://static.vseth.ethz.ch/assets/vseth-0504-exbe/logo-mono-quer.svg"
          logo="/logo-smallicon.svg"
          loginButton={
            <Group>
              {/*<Switch
                size="md"
                color={dark ? 'gray' : 'dark'}
                offLabel={<Icon icon={ICONS.SUN} size={16} color="gray"/>}
                onLabel={<Icon icon={ICONS.MOON} size={16} color="#ffffff"/>}
                checked={dark}
                onChange={() => toggleColorScheme()}
              />*/}
              <LoginButton />
            </Group>
          }
          size="xl"
          signet="/signet_inv.svg"
        >
          <ApolloProvider client={apolloClient}>
            <NotificationsProvider>{props.children}</NotificationsProvider>
          </ApolloProvider>
        </VSETHExternalApp>
      </VSETHThemeProvider>
    </React.StrictMode>
  );
}
