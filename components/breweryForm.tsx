import { useEffect } from "react";

import { useTranslation } from "next-i18next";

import {
  Button,
  Box,
  Checkbox,
  Grid,
  Modal,
  Select,
  Textarea,
  TextInput,
} from "@mantine/core";

import { useForm } from "@mantine/form";

import { gql, useMutation } from "@apollo/client";

const insertBreweryMut = gql`
  mutation insertBrewery(
    $name: String!
    $descriptionDE: String!
    $descriptionEN: String!
    $homepage: String!
    $is_tapped_by_brewery: Boolean!
    $location: String!
  ) {
    insertBrewery(
      name: $name
      descriptionDE: $descriptionDE
      descriptionEN: $descriptionEN
      homepage: $homepage
      is_tapped_by_brewery: $is_tapped_by_brewery
      location: $location
    ) {
      id
    }
  }
`;

const editBreweryMut = gql`
  mutation editBrewery(
    $id: Int!
    $name: String!
    $descriptionDE: String!
    $descriptionEN: String!
    $homepage: String!
    $is_tapped_by_brewery: Boolean!
    $location: String!
  ) {
    editBrewery(
      id: $id
      name: $name
      descriptionDE: $descriptionDE
      descriptionEN: $descriptionEN
      homepage: $homepage
      is_tapped_by_brewery: $is_tapped_by_brewery
      location: $location
    ) {
      id
    }
  }
`;

import BreweryType from "../interfaces/brewery";

export interface BreweryFormProps {
  open: boolean;
  close: () => void;
  brewery: BreweryType | null;
  refetch: any;
}

export default function BreweryForm({
  open,
  close,
  brewery,
  refetch,
}: BreweryFormProps) {
  const { t } = useTranslation("comp.breweryForm");

  const [insertBrewery, { loading: loadingBrewery, error: errorBrewery }] =
    useMutation(insertBreweryMut);
  const [editBrewery, { loading: loadingEdit, error: errorEdit }] =
    useMutation(editBreweryMut);

  const initialValues = {
    name: "",
    descriptionDE: "",
    descriptionEN: "",
    homepage: "",
    is_tapped_by_brewery: true,
    location: "",
  };

  const form = useForm({
    initialValues: initialValues,

    validate: {
      name: (value) => (value != "" ? null : t("invalidValue")),
      descriptionDE: (value) => (value != "" ? null : t("invalidValue")),
      descriptionEN: (value) => (value != "" ? null : t("invalidValue")),
      homepage: (value) => (value != "" ? null : t("invalidValue")),
      location: (value) => (value != "" ? null : t("invalidValue")),
    },
  });

  const setBrewery = () => {
    if (brewery) {
      form.setValues({
        ...brewery,
      });
    } else {
      form.setValues(initialValues);
    }
  };

  useEffect(() => {
    setBrewery();
  }, [brewery]);

  const submit = async (values: any) => {
    if (brewery) {
      await editBrewery({
        variables: {
          id: brewery.id,
          ...values,
        },
      });
    } else {
      await insertBrewery({
        variables: {
          ...values,
        },
      });
    }
    await refetch();
    close();
  };

  return (
    <Modal
      opened={open}
      onClose={close}
      title={<h2>{brewery ? t("editBrewery") : t("addBrewery")}</h2>}
      size="lg"
    >
      <Box>
        <form onSubmit={form.onSubmit((values) => submit(values))}>
          <Grid>
            <Grid.Col md={6} sm={6} xs={12}>
              <TextInput
                withAsterisk
                label={t("lName")}
                placeholder="REIB BIER"
                {...form.getInputProps("name")}
              />
            </Grid.Col>
            <Grid.Col md={6} sm={6} xs={12}>
              <TextInput
                withAsterisk
                label={t("lHomepage")}
                placeholder="https://reibbier.ch"
                {...form.getInputProps("homepage")}
              />
            </Grid.Col>
            <Grid.Col md={6} sm={6} xs={12}>
              <Textarea
                withAsterisk
                label={t("lDescDE")}
                placeholder="REIB BIER ist eine Brauerei, welche..."
                minRows={5}
                {...form.getInputProps("descriptionDE")}
              />
            </Grid.Col>
            <Grid.Col md={6} sm={6} xs={12}>
              <Textarea
                withAsterisk
                label={t("lDescEN")}
                placeholder="REIB BIER is a brewery that..."
                minRows={5}
                {...form.getInputProps("descriptionEN")}
              />
            </Grid.Col>

            <Grid.Col md={6} sm={6} xs={12}>
              <TextInput
                withAsterisk
                label={t("lLocation")}
                placeholder="Kriens LU"
                {...form.getInputProps("location")}
              />
            </Grid.Col>

            <Grid.Col xs={12}>
              <Checkbox
                label={t("lTappedBy")}
                {...form.getInputProps("is_tapped_by_brewery", {
                  type: "checkbox",
                })}
              />
            </Grid.Col>

            <Grid.Col xs={12}>
              <Button type="submit">{t("submit")}</Button>
            </Grid.Col>
          </Grid>
        </form>
      </Box>
    </Modal>
  );
}
