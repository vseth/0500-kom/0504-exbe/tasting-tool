import { useState } from "react";

import { Chip, Grid, Group, Space, TextInput } from "@mantine/core";

import { useDebouncedState } from "@mantine/hooks";

import { Icon, ICONS } from "vseth-canine-ui";

import { gql, useQuery } from "@apollo/client";

import BeerCard from "../components/beerCard";

const AllBeersQuery = gql`
  query {
    beers {
      id
      name
      descriptionDE
      descriptionEN
      type
      brewery {
        id
        name
      }
      price
      alc
      slug
      ratings {
        rating
      }
      is_tapped
    }
  }
`;

const getBreweries = gql`
  query {
    breweries {
      id
      name
      beers {
        id
        is_tapped
      }
    }
  }
`;

import BeerType from "../interfaces/beer";
import BreweryType from "../interfaces/brewery";

export default function BeerCollage() {
  const { data, error, loading } = useQuery(AllBeersQuery);
  const {
    data: breweries,
    error: errorBreweries,
    loading: loadingBreweries,
  } = useQuery(getBreweries);

  const [search, setSearch] = useDebouncedState("", 200);
  const [brewery, setBrewery] = useState<string>("");

  const matchesQuery = (beer: BeerType) => {
    if (search == "") return true;
    return (
      beer.name.toLowerCase() +
      beer.type.toLowerCase() +
      beer.brewery.name.toLowerCase()
    ).includes(search.toLowerCase());
  };

  const matchesBrewery = (beer: BeerType) => {
    if (brewery.length == 0) return true;
    return beer.brewery.id == Number(brewery);
  };

  const setBreweryFilter = (val: string) => {
    if (val == brewery) setBrewery("");
    else setBrewery(val);
    return;
  };

  const hasABeerAvailable = (brewery: BreweryType) => {
    let avail = false;
    brewery.beers.forEach((beer: BeerType) => {
      if (beer.is_tapped) avail = true;
    });
    return avail;
  };

  return (
    <>
      {!data || !breweries ? (
        <Grid>
          <Grid.Col md={4} sm={6} xs={12}>
            <BeerCard />
          </Grid.Col>
          <Grid.Col md={4} sm={6} xs={12}>
            <BeerCard />
          </Grid.Col>
          <Grid.Col md={4} sm={6} xs={12}>
            <BeerCard />
          </Grid.Col>
        </Grid>
      ) : (
        <>
          <TextInput
            label="Filter"
            defaultValue={search}
            onChange={(event) => setSearch(event.currentTarget.value)}
            icon={<Icon icon={ICONS.SEARCH} color="#777777" />}
            size="md"
          />
          <Space h="md" />
          <Chip.Group value={brewery}>
            <Group position="center">
              {breweries.breweries
                .filter((brewery: BreweryType) => hasABeerAvailable(brewery))
                .map((brewery: BreweryType) => (
                  <Chip
                    key={brewery.id}
                    value={String(brewery.id)}
                    onClick={() => setBreweryFilter(String(brewery.id))}
                  >
                    {brewery.name}
                  </Chip>
                ))}
            </Group>
          </Chip.Group>
          <Space h="md" />
          <Grid>
            {data.beers
              .filter((beer: BeerType) => beer.is_tapped)
              .filter((beer: BeerType) => matchesQuery(beer))
              .filter((beer: BeerType) => matchesBrewery(beer))
              .map((beer: BeerType) => (
                <Grid.Col
                  md={4}
                  sm={6}
                  xs={12}
                  style={{ display: "flex" }}
                  key={beer.id}
                >
                  <BeerCard beer={beer} />
                </Grid.Col>
              ))}
          </Grid>
        </>
      )}
    </>
  );
}
