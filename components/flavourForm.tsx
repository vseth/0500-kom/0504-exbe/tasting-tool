import { useState, useEffect } from "react";

import { useTranslation } from "next-i18next";
import { useRouter } from "next/router";

import { useSession, signIn } from "next-auth/react";

import {
  Accordion,
  Button,
  Container,
  Grid,
  Slider,
  Space,
} from "@mantine/core";

import PleaseLogIn from "../components/pleaseLogIn";

import { gql, useMutation } from "@apollo/client";

const MUTATION = gql`
  mutation flavour(
    $sub: String!
    $beerId: Int!
    $hoppy: Float!
    $malty: Float!
    $fruity: Float!
    $bitter: Float!
    $sweet: Float!
    $sour: Float!
  ) {
    flavour(
      sub: $sub
      beerId: $beerId
      hoppy: $hoppy
      malty: $malty
      fruity: $fruity
      bitter: $bitter
      sweet: $sweet
      sour: $sour
    ) {
      id
    }
  }
`;

const MUTATION_UPDATE = gql`
  mutation updateFlavour(
    $id: Int!
    $hoppy: Float!
    $malty: Float!
    $fruity: Float!
    $bitter: Float!
    $sweet: Float!
    $sour: Float!
  ) {
    updateFlavour(
      id: $id
      hoppy: $hoppy
      malty: $malty
      fruity: $fruity
      bitter: $bitter
      sweet: $sweet
      sour: $sour
    ) {
      id
    }
  }
`;

import BeerType from "../interfaces/beer";
import FlavourType from "../interfaces/flavour";

export interface FlavourFormProps {
  beerId: number;
  refetch: any;
  flavours: [FlavourType];
  notify: (title: string, message: string, color: string) => void;
  update: number;
}

export default function FlavourForm({
  beerId,
  refetch,
  flavours,
  notify,
  update,
}: FlavourFormProps) {
  const { t } = useTranslation("comp.flavourForm");
  const { data: session } = useSession();

  let initHoppy = 3;
  let initMalty = 3;
  let initFruity = 3;
  let initBitter = 3;
  let initSweet = 3;
  let initSour = 3;

  const [hoppy, setHoppy] = useState(initHoppy);
  const [malty, setMalty] = useState(initMalty);
  const [fruity, setFruity] = useState(initFruity);
  const [bitter, setBitter] = useState(initBitter);
  const [sweet, setSweet] = useState(initSweet);
  const [sour, setSour] = useState(initSour);

  const [found, setFound] = useState<FlavourType[]>([]);
  const [foundOne, setFoundOne] = useState(false);

  const reloadFlavours = () => {
    if (session && flavours) {
      const foundtmp = flavours.filter(
        (flavour) => flavour.sub == (session as any).info.payload.sub
      );
      setFound(foundtmp);
      if (foundtmp.length > 0) {
        setFoundOne(true);
        setHoppy(foundtmp[0].hoppy);
        setMalty(foundtmp[0].malty);
        setFruity(foundtmp[0].fruity);
        setBitter(foundtmp[0].bitter);
        setSweet(foundtmp[0].sweet);
        setSour(foundtmp[0].sour);
      }
    }
  };

  useEffect(() => {
    reloadFlavours();
  }, [update]);

  const [insertFlavour, { loading, error }] = useMutation(MUTATION);
  const [updateFlavour, { loading: loading_upd, error: error_upd }] =
    useMutation(MUTATION_UPDATE);

  const marks = [
    { value: 1, label: "1" },
    { value: 2, label: "2" },
    { value: 3, label: "3" },
    { value: 4, label: "4" },
    { value: 5, label: "5" },
  ];

  const submit = async () => {
    if (foundOne) {
      await updateFlavour({
        variables: {
          id: found[0].id,
          hoppy,
          malty,
          fruity,
          bitter,
          sweet,
          sour,
        },
      });
      await refetch();
      notify(t("updated"), "", "green");
    } else {
      await insertFlavour({
        variables: {
          sub: (session as any)!.info.payload.sub,
          beerId: beerId,
          hoppy,
          malty,
          fruity,
          bitter,
          sweet,
          sour,
        },
      });
      await refetch();
      notify(t("added"), "", "green");
    }
  };

  return (
    <Accordion.Item value="flavour">
      <Accordion.Control>
        <h2>{t("flavourProfile")}</h2>
      </Accordion.Control>
      <Accordion.Panel>
        <Space h="md" />
        {session ? (
          <Grid>
            <Grid.Col md={6} sm={6} xs={12}>
              <Slider
                value={hoppy}
                onChange={setHoppy}
                label={t("hoppy")}
                labelAlwaysOn
                step={0.5}
                min={1}
                max={5}
                marks={marks}
                sx={{ marginTop: "40px" }}
              />
            </Grid.Col>
            <Grid.Col md={6} sm={6} xs={12}>
              <Slider
                value={malty}
                onChange={setMalty}
                label={t("malty")}
                labelAlwaysOn
                step={0.5}
                min={1}
                max={5}
                marks={marks}
                sx={{ marginTop: "40px" }}
              />
            </Grid.Col>
            <Grid.Col md={6} sm={6} xs={12}>
              <Slider
                value={fruity}
                onChange={setFruity}
                label={t("fruity")}
                labelAlwaysOn
                step={0.5}
                min={1}
                max={5}
                marks={marks}
                sx={{ marginTop: "40px" }}
              />
            </Grid.Col>
            <Grid.Col md={6} sm={6} xs={12}>
              <Slider
                value={bitter}
                onChange={setBitter}
                label={t("bitter")}
                labelAlwaysOn
                step={0.5}
                min={1}
                max={5}
                marks={marks}
                sx={{ marginTop: "40px" }}
              />
            </Grid.Col>
            <Grid.Col md={6} sm={6} xs={12}>
              <Slider
                value={sweet}
                onChange={setSweet}
                label={t("sweet")}
                labelAlwaysOn
                step={0.5}
                min={1}
                max={5}
                marks={marks}
                sx={{ marginTop: "40px" }}
              />
            </Grid.Col>
            <Grid.Col md={6} sm={6} xs={12}>
              <Slider
                value={sour}
                onChange={setSour}
                label={t("sour")}
                labelAlwaysOn
                step={0.5}
                min={1}
                max={5}
                marks={marks}
                sx={{ marginTop: "40px" }}
              />
            </Grid.Col>
            <Grid.Col md={6} sm={6} xs={12}>
              <Button onClick={submit} mt="md">
                {foundOne ? t("update") : t("submit")}
              </Button>
            </Grid.Col>
          </Grid>
        ) : (
          <>
            <PleaseLogIn />
          </>
        )}
        <Space h="md" />
      </Accordion.Panel>
    </Accordion.Item>
  );
}
