import { useSession, signIn, signOut } from "next-auth/react";

import { Button } from "@mantine/core";

import { Icon, ICONS } from "vseth-canine-ui";

const LoginButton = () => {
  const { data: session } = useSession();

  if (session) {
    return (
      <Button
        leftIcon={<Icon icon={ICONS.LEAVE} color={"#ad6300"} />}
        variant="light"
        onClick={() => signOut()}
      >
        Logout
      </Button>
    );
  }
  return (
    <Button
      leftIcon={<Icon icon={ICONS.ENTER} color={"#ad6300"} />}
      variant="light"
      onClick={() => signIn("keycloak")}
    >
      Login
    </Button>
  );
};

export default LoginButton;
