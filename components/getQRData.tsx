import { Button } from "@mantine/core";

import { Icon, ICONS } from "vseth-canine-ui";

import BreweryType from "../interfaces/brewery";

export interface BetQRdataProps {
  breweries: [BreweryType];
}

export default function GetQRdata({ breweries }: BetQRdataProps) {
  const downloadCSV = () => {
    console.log(breweries);
    let text = "";
    breweries.map((brewery) => {
      text += brewery.name + "," + brewery.uuid + "\n";
    });
    let element = document.createElement("a");
    element.setAttribute(
      "href",
      "data:text/plain;charset=utf-8," + encodeURIComponent(text)
    );
    element.setAttribute("download", "QR_Data.csv");

    element.style.display = "none";
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
    return;
  };

  return (
    <Button
      leftIcon={<Icon icon={ICONS.DOWNLOAD} color="white" />}
      onClick={downloadCSV}
    >
      Download QR Code Data
    </Button>
  );
}
