import { useSession, signIn, signOut } from "next-auth/react";

import { useTranslation } from "next-i18next";

import { Text } from "@mantine/core";

export default function PleaseLogIn() {
  const { t } = useTranslation("comp.pleaseSignIn");

  return (
    <Text>
      {t("please")}{" "}
      <span
        onClick={() => signIn("keycloak")}
        style={{ textDecoration: "underline", cursor: "pointer" }}
      >
        {t("login")}
      </span>{" "}
      {t("inOrder")}
    </Text>
  );
}
