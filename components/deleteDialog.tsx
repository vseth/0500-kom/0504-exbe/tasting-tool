import { useTranslation } from "next-i18next";

import { Button, Dialog, Group, Text } from "@mantine/core";

import { Icon, ICONS } from "vseth-canine-ui";

import { gql, useMutation } from "@apollo/client";

const deleteBeerMut = gql`
  mutation deleteBeer($id: Int!) {
    deleteBeer(id: $id) {
      id
    }
  }
`;

const deleteBreweryMut = gql`
  mutation deleteBrewery($id: Int!) {
    deleteBrewery(id: $id) {
      id
    }
  }
`;

import BeerType from "../interfaces/beer";
import BreweryType from "../interfaces/brewery";

export interface RemoveDialogProps {
  open: boolean;
  close: () => void;
  beer: BeerType | null;
  setCurrentBeer?: any;
  brewery?: BreweryType | null;
  setCurrentBrewery?: any;
  isBeerSelected: boolean;
  refetchBeers?: any;
  refetchBreweries: any;
  uuid?: string | string[];
}

export default function RemoveDialog({
  open,
  close,
  beer,
  setCurrentBeer,
  brewery = null,
  setCurrentBrewery = null,
  isBeerSelected,
  refetchBeers = null,
  refetchBreweries,
  uuid = "",
}: RemoveDialogProps) {
  const { t } = useTranslation("comp.deleteDialog");

  const item = isBeerSelected ? beer : brewery;

  const [deleteBeer, { loading: loadingBeer, error: errorBeer }] =
    useMutation(deleteBeerMut);
  const [deleteBrewery, { loading: loadingBrew, error: errorBrew }] =
    useMutation(deleteBreweryMut);

  const remove = async () => {
    if (isBeerSelected) {
      await deleteBeer({
        variables: {
          id: beer!.id,
          uuid: uuid,
        },
      });
      await refetchBeers();
      await refetchBreweries();
      setCurrentBeer(null);
    } else {
      await deleteBrewery({
        variables: {
          id: brewery!.id,
        },
      });
      await refetchBreweries();
      setCurrentBrewery(null);
    }
    close();
  };

  return (
    <Dialog
      opened={open}
      withCloseButton
      onClose={close}
      size="lg"
      radius="md"
      transition="slide-left"
    >
      {item && (
        <>
          {brewery && !isBeerSelected && brewery.beers.length > 0 ? (
            <Text size="md">
              {t("blocker1")} <b>{brewery.name}</b>
              {t("blocker2")}
            </Text>
          ) : (
            <>
              <Text size="md" mb="xs">
                {t("question1")} <b>{item.name}</b>
                {t("question2")}
              </Text>

              <Group align="flex-end">
                <Button
                  leftIcon={<Icon icon={ICONS.DELETE} color="#ffffff" />}
                  onClick={remove}
                  color="red"
                >
                  {t("deleteButton")}
                </Button>
              </Group>
            </>
          )}
        </>
      )}
    </Dialog>
  );
}
