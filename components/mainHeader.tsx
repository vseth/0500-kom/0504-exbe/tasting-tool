import { useRef } from "react";
import Link from "next/link";
import { useTranslation } from "next-i18next";

import {
  Box,
  Button,
  Container,
  Grid,
  Group,
  Image,
  MediaQuery,
  Paper,
} from "@mantine/core";

import { Carousel } from "@mantine/carousel";
import Autoplay from "embla-carousel-autoplay";

import { gql, useQuery } from "@apollo/client";

import config from "../exbeerience.config.js";

import BeerCard from "../components/beerCard";

import { Icon, ICONS } from "vseth-canine-ui";

const AllBeersQuery = gql`
  query {
    beers {
      id
      name
      descriptionDE
      descriptionEN
      type
      brewery {
        name
      }
      price
      alc
      slug
      ratings {
        rating
      }
      is_tapped
    }
  }
`;

import BeerType from "../interfaces/beer";

export default function MainHeader() {
  const { t } = useTranslation("comp.mainHeader");

  const { data, error, loading } = useQuery(AllBeersQuery);
  const autoplay = useRef(Autoplay({ delay: 6000 }));

  return (
    <>
      <Container size="xl">
        <Grid>
          <Grid.Col md={6} sm={12} xs={12} style={{ display: "flex" }}>
            <Box
              style={{
                display: "flex",
                justifyContent: "space-between",
                flexDirection: "column",
                maxWidth: "100%",
              }}
            >
              <div></div>
              <h1 style={{ fontSize: "40pt", fontWeight: "normal" }}>
                Ex<span style={{ fontWeight: "bold" }}>Beer</span>ience{" "}
                {config.year}
              </h1>
              <Paper shadow="md" radius="md">
                <Carousel
                  loop
                  plugins={[autoplay.current]}
                  onMouseEnter={autoplay.current.stop}
                  onMouseLeave={autoplay.current.reset}
                  slideGap="md"
                >
                  {data &&
                    data.beers
                      .filter((beer: BeerType) => beer.is_tapped)
                      .map((beer: BeerType) => (
                        <Carousel.Slide
                          style={{ display: "flex" }}
                          key={beer.id}
                        >
                          <BeerCard beer={beer} />
                        </Carousel.Slide>
                      ))}
                </Carousel>
              </Paper>
              <Link href="/beers" style={{ textDecoration: "none" }}>
                <Group grow>
                  <Button
                    size="xl"
                    rightIcon={<Icon icon={ICONS.RIGHT} color="#ffffff" />}
                    style={{ marginTop: "20px" }}
                    uppercase
                    variant="gradient"
                    gradient={{ from: "#ad6300", to: "#fa9800" }}
                  >
                    {t("browseBeers")}
                  </Button>
                </Group>
              </Link>
              <div></div>
            </Box>
          </Grid.Col>
          <MediaQuery smallerThan="md" styles={{ display: "none" }}>
            <Grid.Col md={6} sm={6} xs={6}>
              <Paper
                shadow="xl"
                radius="xl"
                style={{ overflow: "hidden", position: "relative" }}
              >
                <Image
                  src="/herbstbeerience_24.png"
                  alt="ExBeerience Artwork"
                />
              </Paper>
            </Grid.Col>
          </MediaQuery>
        </Grid>
      </Container>
    </>
  );
}
