// @ts-nocheck
import { useEffect } from "react";

import { useTranslation } from "next-i18next";

import {
  Button,
  Box,
  Checkbox,
  Grid,
  Modal,
  Select,
  Textarea,
  TextInput,
} from "@mantine/core";

import { useForm } from "@mantine/form";

import { gql, useMutation } from "@apollo/client";

const insertBeerMut = gql`
  mutation insertBeer(
    $name: String!
    $type: String!
    $descriptionDE: String!
    $descriptionEN: String!
    $alc: String!
    $slug: String!
    $price: String!
    $breweryId: Int!
    $is_tapped: Boolean!
    $uuid: String
  ) {
    insertBeer(
      name: $name
      type: $type
      descriptionDE: $descriptionDE
      descriptionEN: $descriptionEN
      alc: $alc
      slug: $slug
      price: $price
      breweryId: $breweryId
      is_tapped: $is_tapped
      uuid: $uuid
    ) {
      id
    }
  }
`;

const editBeerMut = gql`
  mutation editBeer(
    $id: Int!
    $name: String!
    $type: String!
    $descriptionDE: String!
    $descriptionEN: String!
    $alc: String!
    $slug: String!
    $price: String!
    $breweryId: Int!
    $is_tapped: Boolean!
    $uuid: String
  ) {
    editBeer(
      id: $id
      name: $name
      type: $type
      descriptionDE: $descriptionDE
      descriptionEN: $descriptionEN
      alc: $alc
      slug: $slug
      price: $price
      breweryId: $breweryId
      is_tapped: $is_tapped
      uuid: $uuid
    ) {
      id
    }
  }
`;

import BeerType from "../interfaces/beer";
import BreweryType from "../interfaces/brewery";

export interface BeerFormProps {
  open: boolean;
  close: () => void;
  beer: BeerType | null;
  breweries: [BreweryType];
  refetch: any;
  uuid?: string | string[];
}

export default function BeerForm({
  open,
  close,
  beer,
  breweries,
  refetch,
  uuid = "",
}: BeerFormProps) {
  const { t } = useTranslation("comp.beerForm");

  const [insertBeer, { loading: loadingBeer, error: errorBeer }] =
    useMutation(insertBeerMut);
  const [editBeer, { loading: loadingEdit, error: errorEdit }] =
    useMutation(editBeerMut);

  const initialValues = {
    name: "",
    type: "",
    descriptionDE: "",
    descriptionEN: "",
    alc: "",
    slug: "",
    price: "",
    breweryId: null,
    is_tapped: false,
  };

  const form = useForm({
    initialValues: initialValues,

    validate: {
      name: (value) => (value != "" ? null : t("invalidValue")),
      type: (value) => (value != "" ? null : t("invalidValue")),
      descriptionDE: (value) => (value != "" ? null : t("invalidValue")),
      descriptionEN: (value) => (value != "" ? null : t("invalidValue")),
      alc: (value) => (value != "" ? null : t("invalidValue")),
      slug: (value) =>
        /( \.[A-Z])/.test(value) || value == "" ? t("invalidValue") : null,
      price: (value) => (value != "" ? null : t("invalidValue")),
      breweryId: (value) => (value ? null : t("invalidValue")),
    },
  });

  const setBeer = () => {
    if (beer) {
      form.setValues({
        ...beer,
        breweryId: beer.brewery.id,
      });
    } else {
      form.setValues(initialValues);
    }
  };

  useEffect(() => {
    setBeer();
  }, [beer]);

  const submit = async (values: any) => {
    if (beer) {
      await editBeer({
        variables: {
          id: beer.id,
          ...values,
          uuid,
        },
      });
    } else {
      await insertBeer({
        variables: {
          ...values,
          uuid,
        },
      });
    }
    await refetch();
    close();
  };

  return (
    <Modal
      opened={open}
      onClose={close}
      title={<h2>{beer ? t("editBeer") : t("addBeer")}</h2>}
      size="lg"
    >
      <Box>
        <form onSubmit={form.onSubmit((values) => submit(values))}>
          <Grid>
            <Grid.Col md={6} sm={6} xs={12}>
              <TextInput
                withAsterisk
                label={t("lName")}
                placeholder="Nullwatt"
                {...form.getInputProps("name")}
              />
            </Grid.Col>
            <Grid.Col md={6} sm={6} xs={12}>
              <TextInput
                withAsterisk
                label={t("lType")}
                placeholder="Birnel Stout"
                {...form.getInputProps("type")}
              />
            </Grid.Col>
            <Grid.Col md={6} sm={6} xs={12}>
              <Textarea
                withAsterisk
                label={t("lDescDE")}
                placeholder="Nullwatt ist ein Stout, welches..."
                minRows={5}
                {...form.getInputProps("descriptionDE")}
              />
            </Grid.Col>
            <Grid.Col md={6} sm={6} xs={12}>
              <Textarea
                withAsterisk
                label={t("lDescEN")}
                placeholder="Nullwatt is a stout that..."
                minRows={5}
                {...form.getInputProps("descriptionEN")}
              />
            </Grid.Col>

            <Grid.Col md={6} sm={6} xs={12}>
              <TextInput
                withAsterisk
                label={t("lAlc")}
                placeholder="4.2"
                icon="%"
                {...form.getInputProps("alc")}
              />
            </Grid.Col>
            <Grid.Col md={6} sm={6} xs={12}>
              <TextInput
                withAsterisk
                label={t("lSlug")}
                placeholder="birnel_stout"
                {...form.getInputProps("slug")}
              />
            </Grid.Col>

            <Grid.Col md={6} sm={6} xs={12}>
              <TextInput
                withAsterisk
                label={t("lPrice")}
                placeholder="1.50"
                icon="CHF"
                {...form.getInputProps("price")}
              />
            </Grid.Col>
            <Grid.Col md={6} sm={6} xs={12}>
              <Select
                withAsterisk
                label={t("lBrewery")}
                placeholder="REIB BIER"
                data={breweries.map((brewery) => {
                  return {
                    label: brewery.name,
                    value: brewery.id,
                  };
                })}
                {...form.getInputProps("breweryId")}
              />
            </Grid.Col>
            <Grid.Col md={6} sm={6} xs={12}>
              <Checkbox
                label={t("lAvailable")}
                {...form.getInputProps("is_tapped", { type: "checkbox" })}
              />
            </Grid.Col>

            <Grid.Col xs={12}>
              <Button type="submit">{t("submit")}</Button>
            </Grid.Col>
          </Grid>
        </form>
      </Box>
    </Modal>
  );
}
