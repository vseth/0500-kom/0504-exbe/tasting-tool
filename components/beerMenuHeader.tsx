import { useRef, useEffect } from "react";
import Link from "next/link";
import { useTranslation } from "next-i18next";

import {
  Box,
  Button,
  Card,
  Container,
  Grid,
  Group,
  Image,
  Paper,
  Space,
} from "@mantine/core";

import { Carousel } from "@mantine/carousel";
import Autoplay from "embla-carousel-autoplay";

import { gql, useQuery } from "@apollo/client";

import config from "../exbeerience.config.js";

import BeerCard from "../components/beerCard";

import { Icon, ICONS } from "vseth-canine-ui";

const AllBeersQuery = gql`
  query {
    beers {
      id
      name
      descriptionDE
      descriptionEN
      type
      brewery {
        name
      }
      price
      alc
      slug
      ratings {
        rating
      }
      is_tapped
    }
  }
`;

import BeerType from "../interfaces/beer";

export default function MainHeader() {
  const { t } = useTranslation("comp.mainHeader");

  const { data, error, loading, refetch } = useQuery(AllBeersQuery);
  const autoplay = useRef(Autoplay({ delay: 6000 }));

  useEffect(() => {
    const interval = setInterval(() => {
      refetch();
    }, 60 * 1000);

    return () => clearInterval(interval); // This represents the unmount function, in which you need to clear your interval to prevent memory leaks.
  }, []);

  return (
    <>
      <Container size="xl">
        <Grid gutter={64}>
          <Grid.Col md={6} sm={12} xs={12} style={{ display: "flex" }}>
            <Box
              style={{
                display: "flex",
                justifyContent: "space-between",
                flexDirection: "column",
                maxWidth: "100%",
              }}
            >
              <h1 style={{ fontSize: "40pt", fontWeight: "normal" }}>
                Herbst<span style={{ fontWeight: "bold" }}>Beer</span>ience{" "}
                {config.year}
              </h1>
              <Paper shadow="md" radius="md">
                <Carousel
                  loop
                  plugins={[autoplay.current]}
                  onMouseEnter={autoplay.current.stop}
                  onMouseLeave={autoplay.current.reset}
                  slideGap="md"
                >
                  {data &&
                    data.beers
                      .filter((beer: BeerType) => beer.is_tapped)
                      .map((beer: BeerType) => (
                        <Carousel.Slide
                          style={{ display: "flex" }}
                          key={beer.id}
                        >
                          <BeerCard beer={beer} />
                        </Carousel.Slide>
                      ))}
                </Carousel>
              </Paper>
              <div></div>
            </Box>
          </Grid.Col>
          <Grid.Col md={6} sm={6} xs={6}>
            <h1 style={{ fontSize: "40pt", fontWeight: "normal" }}>
              Beer Menu
            </h1>
            <h2 style={{ fontSize: "20pt" }}>
              https://tasting.exbeerience.ethz.ch
            </h2>
            <Space h="md" />
            <Paper shadow="xl">
              <Image src="/beerMenu.svg" alt="Beer Menu QR Code" />
            </Paper>
          </Grid.Col>
        </Grid>
      </Container>
    </>
  );
}
