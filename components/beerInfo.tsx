import { Container, Space } from "@mantine/core";

import BeerCard from "../components/beerCard";
import BreweryCard from "../components/breweryCard";

import BeerType from "../interfaces/beer";

export interface BeerInfoProps {
  beer: BeerType;
}

export default function BeerInfo({ beer }: BeerInfoProps) {
  return (
    <>
      <BeerCard beer={beer} showLink={false} />
      <Space h="xl" />
      <BreweryCard brewery={beer.brewery} beerSlug={beer.slug} />
    </>
  );
}
