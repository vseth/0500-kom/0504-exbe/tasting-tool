import { useState } from "react";

import { useSession } from "next-auth/react";

import {
  Avatar,
  Button,
  Container,
  Grid,
  Group,
  Modal,
  Paper,
  Space,
} from "@mantine/core";

import { Icon, ICONS } from "vseth-canine-ui";

import dayjs from "dayjs";
var relativeTime = require("dayjs/plugin/relativeTime");

import hasAccess from "../utilities/hasAccess";

import CommentType from "../interfaces/comment";

export interface BeerCommentsProps {
  comments: [CommentType];
  refetch: any;
}

declare module "dayjs" {
  interface Dayjs {
    fromNow(): any;
  }
}

import { gql, useMutation } from "@apollo/client";

const deleteCommentMut = gql`
  mutation deleteComment($id: Int!) {
    deleteComment(id: $id) {
      id
    }
  }
`;

export default function BeerComments({ comments, refetch }: BeerCommentsProps) {
  dayjs.extend(relativeTime);

  const [opened, setOpened] = useState(false);
  const [commentToDelete, setCommentToDelete] =
    useState<CommentType | null>(null);

  const { data: session } = useSession();

  const [deleteComment, { loading, error }] = useMutation(deleteCommentMut);

  const getInitials = (fn: string, ln: string) => {
    return fn.substring(0, 1) + ln.substring(0, 1);
  };

  const openModal = (comment: CommentType) => {
    setOpened(true);
    setCommentToDelete(comment);
  };

  const removeComment = async () => {
    if (commentToDelete) {
      await deleteComment({
        variables: {
          id: commentToDelete.id,
        },
      });
      refetch();
      setOpened(false);
    }
  };

  return (
    <Container size="xl">
      <Grid>
        {comments.map((comment) => (
          <Grid.Col
            md={4}
            sm={6}
            xs={12}
            style={{ marginTop: "20px" }}
            key={comment.id}
          >
            <div>
              <Group>
                {hasAccess(session, true) ? (
                  <Avatar
                    radius="sm"
                    color="red"
                    size="md"
                    onClick={() => openModal(comment)}
                    style={{ cursor: "pointer" }}
                  >
                    <Icon icon={ICONS.DELETE} color="red" />
                  </Avatar>
                ) : (
                  <Avatar radius="sm" color="vsethMain" size="md">
                    {getInitials(comment.first_name, comment.last_name)}
                  </Avatar>
                )}
                <div>
                  <h2 style={{ marginBottom: "-10px" }}>
                    {comment.first_name} {comment.last_name}
                  </h2>
                  <span style={{ color: "gray", fontSize: "10pt" }}>
                    {dayjs.unix(Number(comment.createdAt) / 1000).fromNow()}
                  </span>
                </div>
              </Group>
              <p style={{ marginTop: "5px" }}>{comment.comment}</p>
            </div>
          </Grid.Col>
        ))}
      </Grid>
      <Modal
        opened={opened}
        onClose={() => setOpened(false)}
        title="Remove Comment"
        centered
      >
        Are you sure you want to delete this comment?
        {commentToDelete && (
          <Paper shadow="xl" p="md" mt="sm" mb="sm">
            <Group>
              <Avatar radius="sm" color="vsethMain" size="md">
                {getInitials(
                  commentToDelete.first_name,
                  commentToDelete.last_name
                )}
              </Avatar>
              <div>
                <h2 style={{ marginBottom: "-10px" }}>
                  {commentToDelete.first_name} {commentToDelete.last_name}
                </h2>
                <span style={{ color: "gray", fontSize: "10pt" }}>
                  {dayjs
                    .unix(Number(commentToDelete.createdAt) / 1000)
                    .fromNow()}
                </span>
              </div>
            </Group>
            <p style={{ marginTop: "5px" }}>{commentToDelete.comment}</p>
          </Paper>
        )}
        <Button
          color="red"
          onClick={removeComment}
          leftIcon={<Icon icon={ICONS.DELETE} color="white" />}
        >
          Delete Comment
        </Button>
      </Modal>
    </Container>
  );
}
