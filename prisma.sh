#!/bin/bash

echo "DATABASE_URL=mysql://$SIP_MYSQL_TASTING_USER:$SIP_MYSQL_TASTING_PW@$SIP_MYSQL_TASTING_SERVER:3306/$SIP_MYSQL_TASTING_NAME?schema=public" > .env

npx prisma generate
npx prisma migrate deploy
