export default function hasAccess(session: any, admin: boolean) {
  const stag_role = "exbe_staging_tasting_exbeerience_tasting";
  const prod_role = "exbe_prod_tasting_exbeerience_tasting";

  if (!session) return false; // not logged in --> no access
  if (!admin) {
    // if no admin is required and is logged in --> access
    return true;
  } else {
    if (!session.info.payload.resource_access) return false; // admin required, but no role --> no access
    if (session.info.payload.resource_access[prod_role]) {
      if (session.info.payload.resource_access[prod_role]?.roles[0] != "board")
        return false; // role incorrect
    } else if (session.info.payload.resource_access[stag_role]) {
      if (session.info.payload.resource_access[stag_role]?.roles[0] != "board")
        return false; // role incorrect
    }
    return true;
  }
}
