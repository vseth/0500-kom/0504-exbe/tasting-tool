import BeerType from './beer';

export default interface FlavourType {
  id: number,
  sub: string,
  hoppy: number,
  malty: number,
  fruity: number,
  bitter: number,
  sweet: number,
  sour: number,
  beer: BeerType,
}