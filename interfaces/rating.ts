import BeerType from './beer';

export default interface RatingType {
  id: number,
  rating: number,
  sub: string,
  beer: BeerType,
}