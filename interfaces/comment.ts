import BeerType from './beer';

export default interface CommentType {
  id: number,
  first_name: string,
  last_name: string,
  comment: string,
  createdAt: string,
  beer: BeerType,
}