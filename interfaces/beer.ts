import BreweryType from './brewery';
import CommentType from './comment';
import RatingType from './rating';
import FlavourType from './flavour';

export default interface BeerType {
  id: number,
  brewery: BreweryType,
  name: string,
  type: string
  descriptionDE: string,
  descriptionEN: string,
  alc: string,
  slug: string,
  price: string,
  createdAt: string,
  updatedAt: string,
  comments: [CommentType],
  ratings: [RatingType],
  flavours: [FlavourType],
  is_tapped: boolean,
}