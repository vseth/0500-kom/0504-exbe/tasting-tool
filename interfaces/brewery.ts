import BeerType from './beer';

export default interface BreweryType {
  id: number,
  name: string,
  descriptionDE: string,
  descriptionEN: string,
  homepage: string,
  is_tapped_by_brewery: boolean,
  beers: [BeerType],
  createdAt: string,
  updatedAt: string,
  location: string,
  uuid: string,
}