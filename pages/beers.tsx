import React from "react";
import Head from "next/head";

import { useTranslation } from "next-i18next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

import BeerCollage from "../components/beerCollage";

import { Container } from "@mantine/core";

export default function Main() {
  const { t } = useTranslation("pages.beers");

  return (
    <>
      <Head>
        <title>Beers | HerbstBeerience 2023</title>
      </Head>
      <Container size="xl">
        <BeerCollage />
      </Container>
    </>
  );
}

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        "pages.beers",
        "comp.beerCard",
        "common",
      ])),
      protected: false,
    },
  };
}
