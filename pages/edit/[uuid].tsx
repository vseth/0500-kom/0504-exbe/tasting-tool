import { useRouter } from "next/router";

import { GetStaticPaths } from "next";
import Head from "next/head";

import { serverSideTranslations } from "next-i18next/serverSideTranslations";

import { gql, useQuery } from "@apollo/client";

import { useTranslation } from "next-i18next";

import { Button, Container, Grid } from "@mantine/core";

import { Icon, ICONS } from "vseth-canine-ui";

import BeerCard from "../../components/beerCard";
import AdminBeers from "../../components/adminBeers";

const breweryByUUID = gql`
  query breweryByUUID($uuid: String!) {
    breweryByUUID(uuid: $uuid) {
      id
      name
      descriptionDE
      descriptionEN
      homepage
      beers {
        id
        name
        type
        descriptionDE
        descriptionEN
        alc
        slug
        price
        is_tapped
        ratings {
          rating
        }
        brewery {
          id
          name
        }
      }
      location
      is_tapped_by_brewery
    }
  }
`;

export default function EditBeers() {
  const router = useRouter();
  const { uuid } = router.query;

  const { t } = useTranslation("pages.edit");

  const { data, error, loading, refetch } = useQuery(breweryByUUID, {
    variables: { uuid },
  });

  return (
    <>
      <Head>
        <title>Edit Beers | HerbstBeerience 2023</title>
      </Head>
      <Container size="xl">
        {data && (
          <>
            <h1>{data.breweryByUUID.name}</h1>
            <AdminBeers
              beers={data.breweryByUUID.beers}
              breweries={[data.breweryByUUID]}
              refetchBeers={refetch}
              refetchBreweries={refetch}
              uuid={uuid}
            />
          </>
        )}
      </Container>
    </>
  );
}

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        "pages.edit",
        "comp.adminBeers",
        "comp.beerCard",
        "comp.deleteDialog",
        "comp.beerForm",
        "common",
      ])),
      protected: false,
    },
  };
}

export const getStaticPaths: GetStaticPaths<{ uuid: string }> = async () => {
  return {
    paths: [], //indicates that no page needs be created at build time
    fallback: "blocking", //indicates the type of fallback
  };
};
