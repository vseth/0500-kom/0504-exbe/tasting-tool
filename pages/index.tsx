import React from "react";
import Head from "next/head";

import { serverSideTranslations } from "next-i18next/serverSideTranslations";

import MainHeader from "../components/mainHeader";

export default function Main() {
  return (
    <>
      <Head>
        <title>HerbstBeerience 2024</title>
      </Head>
      <MainHeader />
    </>
  );
}

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        "comp.beerCard",
        "comp.mainHeader",
        "common",
      ])),
      protected: false,
    },
  };
}
