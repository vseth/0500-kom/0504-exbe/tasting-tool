import React from "react";
import Head from "next/head";

import { serverSideTranslations } from "next-i18next/serverSideTranslations";

import BeerMenuHeader from "../components/beerMenuHeader";

export default function BeerMenu() {
  return (
    <>
      <Head>
        <title>HerbstBeerience 2023</title>
      </Head>
      <BeerMenuHeader />
    </>
  );
}

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        "comp.beerCard",
        "comp.mainHeader",
        "common",
      ])),
      protected: false,
    },
  };
}
