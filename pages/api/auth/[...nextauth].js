import NextAuth from "next-auth";
import KeycloakProvider from "next-auth/providers/keycloak";
const jwt = require("jsonwebtoken");

export const authOptions = {
  providers: [
    KeycloakProvider({
      clientId: process.env.SIP_AUTH_EXBEERIENCE_TASTING_CLIENT_ID,
      clientSecret: process.env.SIP_AUTH_EXBEERIENCE_TASTING_CLIENT_SECRET,
      issuer: process.env.SIP_AUTH_OIDC_ISSUER,
    }),
  ],
  callbacks: {
    jwt({ token, account, profile }) {
      if (account) {
        const tok = account.access_token;
        const decoded = jwt.decode(tok, { complete: true });
        token.info = decoded;
      }
      return token;
    },
    session({ session, token, user }) {
      session.info = token.info;
      return session;
    },
  },
};

export default NextAuth(authOptions);
