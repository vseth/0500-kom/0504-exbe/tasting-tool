// @ts-nocheck
import { createSchema, createYoga } from "graphql-yoga";
import type { NextApiRequest, NextApiResponse } from "next";
import { typeDefs } from "../../graphql/schema";
import { resolvers } from "../../graphql/resolvers";
import { createContext } from "../../graphql/context";

import { getServerSession } from "next-auth/next";
import { authOptions } from "./auth/[...nextauth]";

import type { Session } from "next-auth";

export default createYoga<{
  req: NextApiRequest;
  res: NextApiResponse;
}>({
  schema: createSchema({
    typeDefs,
    resolvers,
    context: createContext,
  }),
  context: async (context) => {
    const session = await getServerSession(
      context.req,
      context.res,
      authOptions
    );

    return {
      session,
    };
  },
  graphqlEndpoint: "/api/graphql",
});

export const config = {
  api: {
    bodyParser: false,
  },
};
