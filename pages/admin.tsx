import { useState } from "react";

import Head from "next/head";

import { useTranslation } from "next-i18next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

import { useSession } from "next-auth/react";

import {
  Button,
  Card,
  Container,
  Dialog,
  Divider,
  Grid,
  Group,
  Space,
} from "@mantine/core";

import { Icon, ICONS } from "vseth-canine-ui";

import BreweryCard from "../components/breweryCard";
import BreweryForm from "../components/breweryForm";
import DeleteDialog from "../components/deleteDialog";
import GetQRData from "../components/getQRData";
import AdminBeers from "../components/adminBeers";
import Forbidden from "../components/forbidden";

import hasAccess from "../utilities/hasAccess";

import { gql, useQuery } from "@apollo/client";

const getBeers = gql`
  query {
    beers {
      id
      name
      type
      descriptionDE
      descriptionEN
      alc
      slug
      price
      is_tapped
      brewery {
        id
        name
      }
      ratings {
        rating
      }
    }
  }
`;

const getBreweries = gql`
  query {
    breweries {
      id
      name
      descriptionDE
      descriptionEN
      is_tapped_by_brewery
      homepage
      location
      beers {
        id
        name
        slug
        type
        is_tapped
      }
      uuid
    }
  }
`;

import BreweryType from "../interfaces/brewery";

export default function AdminPage() {
  const { t } = useTranslation("pages.admin");

  const [breweryFormOpen, setBreweryFormOpen] = useState(false);

  const [currentBeer, setCurrentBeer] = useState(null);
  const [currentBrewery, setCurrentBrewery] =
    useState<BreweryType | null>(null);

  const [isBeerSelected, setIsBeerSelected] = useState(false);
  const [isDeleteFormOpen, setIsDeleteFormOpen] = useState(false);

  const {
    data: beers,
    error: errorBeers,
    loading: loadingBeers,
    refetch: refetchBeers,
  } = useQuery(getBeers);
  const {
    data: breweries,
    error,
    loading,
    refetch: refetchBreweries,
  } = useQuery(getBreweries);

  const { data: session } = useSession();

  if (!hasAccess(session, true)) {
    return <Forbidden />;
  }

  return (
    <>
      <Head>
        <title>Admin | ExBeerience 2023</title>
      </Head>
      <Container size="xl">
        <h1>{t("title")}</h1>

        {breweries && beers && (
          <AdminBeers
            beers={beers.beers}
            refetchBeers={refetchBeers}
            breweries={breweries.breweries}
            refetchBreweries={refetchBreweries}
          />
        )}

        <Space h="md" />
        <Divider my="xl" label="//" labelPosition="center" />
        <Space h="xl" />

        {breweries && (
          <Group>
            <Button
              leftIcon={<Icon icon={ICONS.PLUS} color="#ffffff" />}
              onClick={() => setBreweryFormOpen(true)}
            >
              {t("addBrewery")}
            </Button>
            <GetQRData breweries={breweries.breweries} />
          </Group>
        )}

        <Space h="md" />

        <Grid>
          {breweries &&
            breweries.breweries.map((brewery: BreweryType) => (
              <Grid.Col
                md={4}
                sm={6}
                xs={12}
                style={{ display: "flex" }}
                key={brewery.id}
              >
                <BreweryCard
                  brewery={brewery}
                  admin={true}
                  editButton={
                    <Button
                      leftIcon={<Icon icon={ICONS.EDIT} color="#ffffff" />}
                      onClick={() => {
                        setCurrentBrewery(brewery);
                        setBreweryFormOpen(true);
                      }}
                    >
                      {t("edit")}
                    </Button>
                  }
                  deleteButton={
                    <Button
                      leftIcon={<Icon icon={ICONS.DELETE} color="#ffffff" />}
                      color="red"
                      onClick={() => {
                        setCurrentBrewery(brewery);
                        setIsBeerSelected(false);
                        setIsDeleteFormOpen(true);
                      }}
                    >
                      {t("delete")}
                    </Button>
                  }
                />
              </Grid.Col>
            ))}
        </Grid>

        <BreweryForm
          open={breweryFormOpen}
          brewery={currentBrewery}
          close={() => {
            setBreweryFormOpen(false);
            setCurrentBrewery(null);
          }}
          refetch={refetchBreweries}
        />

        <DeleteDialog
          open={isDeleteFormOpen}
          close={() => setIsDeleteFormOpen(false)}
          beer={currentBeer}
          brewery={currentBrewery}
          setCurrentBrewery={setCurrentBrewery}
          isBeerSelected={false}
          refetchBreweries={refetchBreweries}
        />
      </Container>
    </>
  );
}

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        "pages.admin",
        "comp.adminBeers",
        "comp.beerCard",
        "comp.breweryCard",
        "comp.deleteDialog",
        "comp.breweryForm",
        "comp.beerForm",
        "comp.forbidden",
        "common",
      ])),
      protected: false,
    },
  };
}
