import { useState } from "react";

import { useRouter } from "next/router";
import Head from "next/head";
import { GetStaticPaths } from "next";
import Link from "next/link";

import { useTranslation } from "next-i18next";

import { serverSideTranslations } from "next-i18next/serverSideTranslations";

import { useSession } from "next-auth/react";

import BeerInfo from "../../components/beerInfo";
import BeerComment from "../../components/beerComment";
import BeerComments from "../../components/beerComments";
import BeerRating from "../../components/beerRating";
import FlavourForm from "../../components/flavourForm";
import FlavourSpider from "../../components/flavourSpider";

import {
  Accordion,
  Button,
  Container,
  Grid,
  MediaQuery,
  Space,
} from "@mantine/core";

import { Icon, ICONS } from "vseth-canine-ui";

import { showNotification } from "@mantine/notifications";

import { gql, useQuery } from "@apollo/client";

export default function BeerInfoPage() {
  const router = useRouter();
  const { slug } = router.query;

  const { t } = useTranslation("pages.beerSlug");

  const { data: session } = useSession();

  const [accValue, setAccValue] = useState<string | null>(null);
  const [update, setUpdate] = useState(0);

  const BeerQuery = gql`
    query Beer($slug: String!) {
      beerBySlug(slug: $slug) {
        id
        name
        descriptionDE
        descriptionEN
        type
        brewery {
          name
          descriptionDE
          descriptionEN
          homepage
          beers {
            name
            slug
            type
            is_tapped
          }
          location
          is_tapped_by_brewery
        }
        price
        alc
        slug
        comments {
          id
          createdAt
          first_name
          last_name
          comment
        }
        ratings {
          id
          sub
          rating
        }
        flavours {
          id
          sub
          hoppy
          malty
          fruity
          bitter
          sweet
          sour
        }
        is_tapped
      }
    }
  `;

  const notify = (title: string, message: string, color: string) => {
    showNotification({
      title,
      message,
      color,
      autoClose: 5000,
    });
  };

  const { data, error, loading, refetch } = useQuery(BeerQuery, {
    variables: { slug },
    onCompleted: () => setUpdate(update + 1),
  });

  if (error) console.log(error);

  if (!data) {
    return <></>;
  }

  return (
    <>
      <Head>
        <title>Beer Info | HerbstBeerience 2023</title>
      </Head>
      <Container size="xl">
        <MediaQuery largerThan="md" styles={{ display: "none" }}>
          <div>
            <Link href="/beers">
              <Button
                leftIcon={<Icon icon={ICONS.LEFT} color="#ad6300" />}
                variant="light"
              >
                {t("backToBeerMenu")}
              </Button>
            </Link>
            <Space h="md" />
          </div>
        </MediaQuery>
        <Grid>
          <Grid.Col md={6} sm={6} xs={12}>
            <BeerInfo beer={data.beerBySlug} />
          </Grid.Col>
          <Grid.Col md={6} sm={6} xs={12} style={{ display: "flex" }}>
            <FlavourSpider
              flavours={data.beerBySlug.flavours}
              setAccValue={setAccValue}
            />
          </Grid.Col>
        </Grid>

        <Space h="xl" />
        <Space h="xl" />

        <div id="flavourProfile"></div>
        <Accordion value={accValue} onChange={setAccValue}>
          <FlavourForm
            beerId={data.beerBySlug.id}
            refetch={refetch}
            flavours={data.beerBySlug.flavours}
            notify={notify}
            update={update}
          />
          <BeerComment
            beerId={data.beerBySlug.id}
            refetch={refetch}
            notify={notify}
          />
          <BeerRating
            beerId={data.beerBySlug.id}
            refetch={refetch}
            ratings={data.beerBySlug.ratings}
            notify={notify}
            update={update}
          />
        </Accordion>
      </Container>

      <Space h="xl" />

      <BeerComments comments={data.beerBySlug.comments} refetch={refetch} />
    </>
  );
}

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        "pages.beerSlug",
        "comp.beerCard",
        "comp.breweryCard",
        "comp.flavourForm",
        "comp.beerComment",
        "comp.beerRating",
        "comp.flavourSpider",
        "comp.pleaseSignIn",
        "common",
      ])),
      protected: false,
    },
  };
}

export const getStaticPaths: GetStaticPaths<{ slug: string }> = async () => {
  return {
    paths: [], //indicates that no page needs be created at build time
    fallback: "blocking", //indicates the type of fallback
  };
};
