import React, { useState } from "react";

import type { AppProps } from "next/app";
import Head from "next/head";

import { SessionProvider } from "next-auth/react";

import { appWithTranslation } from "next-i18next";

import { ColorSchemeProvider, ColorScheme } from "@mantine/core";

import Navbar from "../components/navbar";

import "../styles/globals.css";

function App({ Component, pageProps: { session, ...pageProps } }: AppProps) {
  const [colorScheme, setColorScheme] = useState<ColorScheme>("light");
  const toggleColorScheme = (value?: ColorScheme) =>
    setColorScheme(value || (colorScheme === "dark" ? "light" : "dark"));

  return (
    <SessionProvider session={session}>
      <Head>
        <meta name="description" content="ExBeerience Tasting Application" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.png" />
      </Head>
      <ColorSchemeProvider
        colorScheme={colorScheme}
        toggleColorScheme={toggleColorScheme}
      >
        <Navbar>
          <Component {...pageProps} />
        </Navbar>
      </ColorSchemeProvider>
    </SessionProvider>
  );
}

export default appWithTranslation(App);
