import prisma from '../lib/prisma';
import { getServerSession } from 'next-auth/next';
import { authOptions } from '../pages/api/auth/[...nextauth]';
import hasAccess from '../utilities/hasAccess';

const checkUUID = async (breweryId: number, uuid: string) => {
  const brewery = await prisma.brewery.findUnique({
   where: {
     id: breweryId
   }
  });
  if(!brewery) return false;
  if(brewery.uuid === uuid) return true;
  return false;
}

export interface beerByIdProps {
  id: number;
}

export interface beerBySlugProps {
  slug: string;
}

export interface breweryByUUIDProps {
  uuid: string;
}

export interface updateRatingProps {
  id: number;
  rating: number;
}

export interface flavourProps {
  sub: string;
  beerId: number;
  hoppy: number;
  malty: number;
  fruity: number;
  bitter: number;
  sweet: number;
  sour: number;
}

export interface updateFlavourProps {
  id: number;
  hoppy: number;
  malty: number;
  fruity: number;
  bitter: number;
  sweet: number;
  sour: number;
}

export interface commentProps {
  first_name: string;
  last_name: string;
  comment: string;
  beerId: number;
}

export interface ratingProps {
  sub: string;
  rating: number;
  beerId: number;
}

export interface insertBeerProps {
  name: string;
  type: string;
  descriptionDE: string;
  descriptionEN: string;
  alc: string;
  slug: string;
  price: string;
  breweryId: number;
  is_tapped: boolean;
  uuid: string;
}

export interface editBeerProps {
  id: number;
  name: string;
  type: string;
  descriptionDE: string;
  descriptionEN: string;
  alc: string;
  slug: string;
  price: string;
  breweryId: number;
  is_tapped: boolean;
  uuid: string;
}

export interface deleteBeerProps {
  id: number;
  uuid: string;
}

export interface insertBreweryProps {
  name: string;
  descriptionDE: string;
  descriptionEN: string;
  homepage: string;
  is_tapped_by_brewery: boolean;
  location: string;
}

export interface editBreweryProps {
  id: number;
  name: string;
  descriptionDE: string;
  descriptionEN: string;
  homepage: string;
  is_tapped_by_brewery: boolean;
  location: string;
}

export interface deleteBreweryProps {
  id: number;
}

export interface deleteCommentProps {
  id: number;
}

export const resolvers = {
  Query: {
    beers: async () => await prisma.beer.findMany({
      include: { brewery: true, ratings: true },
      orderBy: [
        { name: 'asc' },
        { brewery: {
          name: 'asc'
        }}
      ]
    }),
    beerById: async (_: any, {id}: beerByIdProps) => await prisma.beer.findUnique({
      where: { id: Number(id) },
      include: {
        brewery: true,
      },
    }),
    beerBySlug: async (_: any, {slug}: beerBySlugProps) => await prisma.beer.findFirst({
      where: { slug: slug },
      include: {
        comments: true,
        ratings: true,
        flavours: true,
        brewery: {
          include: {
            beers: true,
          }
        },
      },
    }),
    breweries: async () => await prisma.brewery.findMany({
      include: { beers: true },
      orderBy: { name: 'asc' },
    }),
    brewery: async () => await prisma.brewery.findMany(),
    breweryByUUID: async (_: any, {uuid}: breweryByUUIDProps) => await prisma.brewery.findUnique({
      where: {
        uuid: uuid
      },
      include: {
        beers: {
          include: {
            ratings: true,
            brewery: true,
          }
        }
      }
    }),
  },
  Mutation: {
    comment: async (_: any, { first_name, last_name, comment, beerId }: commentProps) => {
      const entity = await prisma.comment.create({
        data: {
          first_name,
          last_name,
          comment,
          beerId,
        }
      });
      return entity;
    },
    rating: async(_: any, { sub, rating, beerId }: ratingProps) => {
      const entity = await prisma.rating.create({
        data: {
          rating,
          beerId,
          sub,
        }
      });
      return entity;
    },
    updateRating: async(_: any, { id, rating }: updateRatingProps) => {
      const entity = await prisma.rating.update({
        where: {
          id
        },
        data: {
          rating
        }  
      });
      return entity;
    },
    flavour: async(_: any, { sub, beerId, hoppy, malty, fruity, bitter, sweet, sour }: flavourProps) => {
      const entity = await prisma.flavour.create({
        data: {
          sub, beerId, hoppy, malty, fruity, bitter, sweet, sour
        }
      });
      return entity;
    },
    updateFlavour: async(_: any, { id, hoppy, malty, fruity, bitter, sweet, sour }: updateFlavourProps) => {
      const entity = await prisma.flavour.update({
        where: {
          id
        },
        data: {
          hoppy, malty, fruity, bitter, sweet, sour
        }
      });
      return entity;
    },
    insertBeer: async(_: any, { name, type, descriptionDE, descriptionEN, alc, slug, price, breweryId, is_tapped, uuid }: insertBeerProps, { session }: any) => {
      const validUUID = await checkUUID(breweryId, uuid);
      const validUser = await hasAccess(session, true);

      if(!validUUID && !validUser) return;

      const entity = await prisma.beer.create({
        data: {
          name, type, descriptionDE, descriptionEN, alc, slug, price, breweryId, is_tapped
        }
      });
      return entity;
    },
    editBeer: async(_: any, { id, name, type, descriptionDE, descriptionEN, alc, slug, price, breweryId, is_tapped, uuid }: editBeerProps, { session }: any) => {
      const validUUID = await checkUUID(breweryId, uuid);
      const validUser = await hasAccess(session, true);

      if(!validUUID && !validUser) return;

      const entity = await prisma.beer.update({
        where: { id },
        data: {
          name, type, descriptionDE, descriptionEN, alc, slug, price, breweryId, is_tapped
        }
      });
      return entity;
    },
    deleteBeer: async(_: any, { id, uuid }: deleteBeerProps, { session }: any) => {
      const validUUID = await checkUUID(id, uuid);
      const validUser = await hasAccess(session, true);

      if(!validUUID && !validUser) return;

      const entity = await prisma.beer.delete({
        where: { id },
      });
      return entity;
    },
    insertBrewery: async(_: any, { name, descriptionDE, descriptionEN, homepage, is_tapped_by_brewery, location }: insertBreweryProps, { session }: any) => {
      const validUser = await hasAccess(session, true);
      if(!validUser) return;

      const entity = await prisma.brewery.create({
        data: {
          name, descriptionDE, descriptionEN, homepage, is_tapped_by_brewery, location
        }
      });
      return entity;
    },
    editBrewery: async(_: any, { id, name, descriptionDE, descriptionEN, homepage, is_tapped_by_brewery, location }: editBreweryProps, { session }: any) => {
      const validUser = await hasAccess(session, true);
      if(!validUser) return;

      const entity = await prisma.brewery.update({
        where: { id },
        data: {
          name, descriptionDE, descriptionEN, homepage, is_tapped_by_brewery, location
        }
      });
      return entity;
    },
    deleteBrewery: async(_: any, { id }: deleteBreweryProps, { session }: any) => {
      const validUser = await hasAccess(session, true);
      if(!validUser) return;

      const entity = await prisma.brewery.delete({
        where: { id },
      });
      return entity;
    },
    deleteComment: async(_: any, { id }: deleteCommentProps, { session }: any) => {
      const validUser = await hasAccess(session, true);
      if(!validUser) return;

      const entity = await prisma.comment.delete({
        where: { id },
      });
      return entity;
    },
  }
}
