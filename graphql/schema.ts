export const typeDefs = `
  type Beer {
    id: Int
    name: String
    type: String
    descriptionDE: String
    descriptionEN: String
    alc: String
    slug: String
    price: String
    createdAt: String
    updatedAt: String
    brewery: Brewery 
    comments: [Comment]
    ratings: [Rating]
    flavours: [Flavour]
    is_tapped: Boolean
  }

  type Brewery {
    id: Int
    name: String
    descriptionDE: String
    descriptionEN: String
    homepage: String
    is_tapped_by_brewery: Boolean
    beers: [Beer]
    createdAt: String
    updatedAt: String
    location: String
    uuid: String
  }

  type Comment {
    id: Int
    first_name: String
    last_name: String
    comment: String
    createdAt: String
    beer: Beer
  }

  type Rating {
    id: Int
    sub: String
    beer: Beer
    rating: Float
  }
  
  type Flavour {
    id: Int
    sub: String 
    beer: Beer
    hoppy: Float
    malty: Float
    fruity: Float
    bitter: Float
    sweet: Float
    sour: Float
  }
  
  type Query {
    beers: [Beer]!
    beerById(id: Int!): Beer
    beerBySlug(slug: String!): Beer
    breweries: [Brewery]!
    brewery(id: Int!): Brewery
    breweryByUUID(uuid: String!): Brewery
  }

  type Mutation {
    comment(first_name: String!, last_name: String!, comment: String!, beerId: Int): Comment
    rating(sub: String!, rating: Float!, beerId: Int!): Rating
    updateRating(id: Int!, rating: Float!): Rating
    flavour(sub: String!, beerId: Int!, hoppy: Float!, malty: Float!, fruity: Float!, bitter: Float!, sweet: Float!, sour: Float!): Flavour
    updateFlavour(id: Int!, hoppy: Float!, malty: Float!, fruity: Float!, bitter: Float!, sweet: Float!, sour: Float!): Flavour
    insertBeer(name: String!, type: String!, descriptionDE: String!, descriptionEN: String!, alc: String!, slug: String!, price: String!, breweryId: Int!, is_tapped: Boolean!, uuid: String): Beer
    editBeer(id: Int!, name: String!, type: String!, descriptionDE: String!, descriptionEN: String!, alc: String!, slug: String!, price: String!, breweryId: Int!, is_tapped: Boolean!, uuid: String): Beer
    deleteBeer(id: Int!, uuid: String): Beer
    insertBrewery(name: String!, descriptionDE: String!, descriptionEN: String!, homepage: String!, is_tapped_by_brewery: Boolean!, location: String!): Brewery
    editBrewery(id: Int!, name: String!, descriptionDE: String!, descriptionEN: String!, homepage: String!, is_tapped_by_brewery: Boolean!, location: String!): Brewery
    deleteBrewery(id: Int!): Brewery
    deleteComment(id: Int!): Comment
  }
`;

